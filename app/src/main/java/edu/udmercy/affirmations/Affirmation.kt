package edu.udmercy.affirmations

data class Affirmation(val stringResourceId: Int)